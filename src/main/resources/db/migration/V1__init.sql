-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema financeiro
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema financeiro
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `financeiro` DEFAULT CHARACTER SET utf8 ;
USE `financeiro` ;

-- -----------------------------------------------------
-- Table `financeiro`.`categoria_movimentacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `financeiro`.`categoria_movimentacao` (
  `idcategoria_movimentacao` BIGINT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `descricao` VARCHAR(255) NULL,
  `tipo` CHAR(1) NOT NULL COMMENT 'E-ENTRADA,\nS-SAIDA',
  PRIMARY KEY (`idcategoria_movimentacao`),
  UNIQUE INDEX `nome_UNIQUE` (`nome` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `financeiro`.`movimentacoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `financeiro`.`movimentacoes` (
  `idmovimentacoes` BIGINT ZEROFILL NOT NULL,
  `valor` DECIMAL NOT NULL,
  `categoria_id` BIGINT NOT NULL,
  `igreja_id` BIGINT NOT NULL,
  `data` DATE NOT NULL,
  PRIMARY KEY (`idmovimentacoes`),
  INDEX `fk_movimentacoes_categoria_movimentacao1_idx` (`categoria_id` ASC),
  CONSTRAINT `fk_movimentacoes_categoria_movimentacao1`
    FOREIGN KEY (`categoria_id`)
    REFERENCES `financeiro`.`categoria_movimentacao` (`idcategoria_movimentacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `financeiro`.`pagamento_dizimo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `financeiro`.`pagamento_dizimo` (
  `idpagamento_dizimo` BIGINT NOT NULL AUTO_INCREMENT,
  `membro_id` BIGINT NOT NULL,
  `valor` DECIMAL NOT NULL,
  `data` DATE NOT NULL,
  PRIMARY KEY (`idpagamento_dizimo`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
