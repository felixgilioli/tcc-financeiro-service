package br.com.felix.financeiroservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@EnableEurekaClient
@SpringBootApplication
class FinanceiroServiceApplication

fun main(args: Array<String>) {
	runApplication<FinanceiroServiceApplication>(*args)
}

