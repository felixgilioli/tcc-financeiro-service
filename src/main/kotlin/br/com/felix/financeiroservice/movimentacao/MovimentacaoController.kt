package br.com.felix.financeiroservice.movimentacao

import br.com.felix.financeiroservice.core.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("movimentacao")
class MovimentacaoController(private val service: MovimentacaoService) : CrudController<Movimentacao, Long>() {

    override fun getService() = service
}