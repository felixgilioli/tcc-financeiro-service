package br.com.felix.financeiroservice.movimentacao

import org.springframework.data.jpa.repository.JpaRepository

interface MovimentacaoRepository : JpaRepository<Movimentacao, Long> {
}