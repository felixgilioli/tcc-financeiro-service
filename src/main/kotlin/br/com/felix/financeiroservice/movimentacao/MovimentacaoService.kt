package br.com.felix.financeiroservice.movimentacao

import br.com.felix.financeiroservice.core.crud.CrudService

interface MovimentacaoService : CrudService<Movimentacao, Long> {
}