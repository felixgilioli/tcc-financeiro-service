package br.com.felix.financeiroservice.movimentacao

import br.com.felix.financeiroservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class MovimentacaoServiceImpl(private val repository: MovimentacaoRepository) : MovimentacaoService, CrudServiceImpl<Movimentacao, Long>() {

    override fun getRepository() = repository
}