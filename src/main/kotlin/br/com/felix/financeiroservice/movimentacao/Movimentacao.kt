package br.com.felix.financeiroservice.movimentacao

import br.com.felix.financeiroservice.categoria.Categoria
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "movimentacoes")
data class Movimentacao(

        @NotNull(message = "valor nao pode ser nulo.")
        @Column(nullable = false)
        val valor: BigDecimal,

        @NotNull(message = "categoria nao pode ser nula.")
        @ManyToOne(optional = false)
        @JoinColumn(name = "categoria_id", nullable = false)
        val categoria: Categoria,

        @NotNull(message = "igrejaId nao pode ser nula.")
        @Column(name = "igreja_id", nullable = false)
        val igrejaId: Long,

        @NotNull(message = "data nao pode ser nula.")
        @Column(nullable = false)
        val data: LocalDate,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idmovimentacoes")
        val idMovimentacao: Long? = null

)