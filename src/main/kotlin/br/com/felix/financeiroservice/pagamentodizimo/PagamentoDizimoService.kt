package br.com.felix.financeiroservice.pagamentodizimo

import br.com.felix.financeiroservice.core.crud.CrudService

interface PagamentoDizimoService : CrudService<PagamentoDizimo, Long> {
}