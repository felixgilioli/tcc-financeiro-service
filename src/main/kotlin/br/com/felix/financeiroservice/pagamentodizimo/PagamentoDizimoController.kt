package br.com.felix.financeiroservice.pagamentodizimo

import br.com.felix.financeiroservice.core.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("pagamentodizimo")
class PagamentoDizimoController(private val service: PagamentoDizimoService) : CrudController<PagamentoDizimo, Long>() {

    override fun getService() = service
}