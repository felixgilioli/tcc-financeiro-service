package br.com.felix.financeiroservice.pagamentodizimo

import br.com.felix.financeiroservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class PagamentoDizimoServiceImpl(private val repository: PagamentoDizimoRepository)
    : PagamentoDizimoService, CrudServiceImpl<PagamentoDizimo, Long>() {

    override fun getRepository() = repository
}