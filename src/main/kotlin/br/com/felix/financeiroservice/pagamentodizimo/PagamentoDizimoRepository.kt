package br.com.felix.financeiroservice.pagamentodizimo

import org.springframework.data.jpa.repository.JpaRepository

interface PagamentoDizimoRepository : JpaRepository<PagamentoDizimo, Long> {
}