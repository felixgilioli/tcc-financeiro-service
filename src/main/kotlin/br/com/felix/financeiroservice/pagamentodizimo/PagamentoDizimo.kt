package br.com.felix.financeiroservice.pagamentodizimo

import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
data class PagamentoDizimo(

        @NotNull(message = "membroId nao pode ser nulo.")
        @Column(name = "membro_id", nullable = false)
        val membroId: Long,

        @NotNull(message = "valor nao pode ser nulo.")
        @Column(nullable = false)
        val valor: BigDecimal,

        @NotNull(message = "data nao pode ser nula.")
        @Column(nullable = false)
        val data: LocalDate,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idpagamento_dizimo")
        val idPagamentoDizimo: Long? = null
)