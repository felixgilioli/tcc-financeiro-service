package br.com.felix.financeiroservice.core.crud

import org.springframework.web.bind.annotation.*
import java.io.Serializable
import javax.validation.Valid

abstract class CrudController<T, ID: Serializable> {

    abstract fun getService() : CrudService<T, ID>

    @GetMapping("{id}")
    fun findById(@PathVariable id: ID) = getService().findById(id)

    @GetMapping
    fun findAll() = getService().findAll()

    @PostMapping
    fun save(@Valid @RequestBody entity: T) = getService().save(entity)

    @DeleteMapping("{id}")
    fun delete(@PathVariable id: ID) = getService().delete(id)

    @DeleteMapping
    fun delete(@RequestBody entity: T) = getService().delete(entity)
}
