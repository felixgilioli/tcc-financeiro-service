package br.com.felix.financeiroservice.core.crud

import java.io.Serializable

interface CrudService<T, ID : Serializable> {

    fun findById(id: ID) : T?

    fun findAll() : List<T>?

    fun save(entity: T) : T

    fun saveAll(entities: List<T>) : List<T>

    fun delete(entity: T)

    fun delete(id: ID)

}
