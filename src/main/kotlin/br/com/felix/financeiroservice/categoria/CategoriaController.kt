package br.com.felix.financeiroservice.categoria

import br.com.felix.financeiroservice.core.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("categoria")
class CategoriaController(private val service: CategoriaService) : CrudController<Categoria, Long>() {

    override fun getService() = service
}