package br.com.felix.financeiroservice.categoria

import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "categoria_movimentacao")
data class Categoria(

        @Length(min = 2, max = 100, message = "nome deve estar entre 2 e 100 caracteres.")
        @Column(nullable = false, length = 100)
        val nome: String,

        @Length(max = 255, message = "descricao nao pode ultrapassar 255 caracteres.")
        @Column(length = 255)
        val descricao: String,

        @NotNull(message = "tipo nao pode ser nulo.")
        @Enumerated(EnumType.STRING)
        @Column(nullable = false, length = 1, columnDefinition = "char(1)")
        val tipo: TipoCategoria,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idcategoria_movimentacao")
        val idCategoria: Long? = null

)