package br.com.felix.financeiroservice.categoria

enum class TipoCategoria(private val label: String) {
    E("Entrada"),
    S("Saida")
}