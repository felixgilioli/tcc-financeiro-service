package br.com.felix.financeiroservice.categoria

import br.com.felix.financeiroservice.core.crud.CrudService

interface CategoriaService : CrudService<Categoria, Long> {
}