package br.com.felix.financeiroservice.categoria

import br.com.felix.financeiroservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class CategoriaServiceImpl(private val repository: CategoriaRepository) : CategoriaService, CrudServiceImpl<Categoria, Long>() {

    override fun getRepository() = repository
}